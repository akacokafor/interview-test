@component('mail::message')
    # Sample Email for Invoice Generated

    New Invoice has been generated!


    @component('mail::table')
        | Reference     | Amount |
        | ------------- | --------:|
        | {{$invoice->reference}}     | $10      |
    @endcomponent

    Thanks
    {{ config('app.name') }}
@endcomponent
