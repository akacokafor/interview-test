<?php

namespace App\Mail;

use App\Models\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceGenerated extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Invoice
     */
    public $invoice;
    private $pdfFilePath;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice, $pdfFilePath)
    {
        $this->invoice = $invoice;
        $this->pdfFilePath = $pdfFilePath;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.invoices.new')
            ->attachFromStorage($this->pdfFilePath, 'name.pdf', [
                'mime' => 'application/pdf'
            ]);
    }
}
